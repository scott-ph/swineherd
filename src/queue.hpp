/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <deque>
#include <memory>
#include <mutex>
#include <condition_variable>

namespace swineherd {

struct QueueExhausted : public std::exception {

const char *
what() const throw() {
    return "Queue is closed and empty";
}

};

template <class T>
class Queue {
public:
    Queue() {};
    ~Queue() {};

    void put(T thing) {
        std::lock_guard<std::mutex> lk(wlock);
        if (is_closed) {
            // XXX: have a real error or something
            abort();
        }
        queue.push_back(std::move(thing));
        cv.notify_one();
    };

    T get() {
        std::unique_lock<std::mutex> lk(rlock);
        cv.wait(lk, [this]{ return !queue.empty() or (is_closed and queue.empty()); });
        if (is_closed and queue.empty()) {
            throw QueueExhausted();
        }
        T ret;
        std::swap(ret, queue.front());
        queue.pop_front();
        return ret;
    };

    void close() {
        // Required in case someone is trying to put something on the queue when we close it
        std::lock_guard<std::mutex> lk(wlock);
        is_closed = true;
        cv.notify_all();  // flush any reamining threads.
    };

    bool closed() {
        // Avoid races with the close method
        std::lock_guard<std::mutex> lk(wlock);
        return bool(is_closed);
    };

    bool empty() {
        // Avoid races
        std::lock_guard<std::mutex> lk(rlock);
        std::lock_guard<std::mutex> lc(wlock);
        return queue.empty();
    };

private:
    std::mutex rlock;
    std::mutex wlock;
    std::condition_variable cv;
    std::deque<T> queue;
    bool is_closed = false;
};

}
