/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string>
#include <list>
#include <map>
#include <memory>
#include <thread>
#include <tuple>
#include <iostream>

#include "logger.hpp"
#include "runner.hpp"
#include "profile.hpp"
#include "options.hpp"

#include "utils.hpp"
#include "time.hpp"
#include "backends/json.hpp"
#include "tests/base.hpp"
#include "tests/piglit_waffle.hpp"

using namespace swineherd;

// Threads for non-test running
#define UTILITY_THREADS 1

namespace {

std::map<std::string, std::string> 
getEnvironment() {
    std::map<std::string, std::string> map{};
    std::string out, err;
    int ret;

#define CALL_ENV_HELPER(name)                          \
    std::tie(out, err, ret) = utils::subProcess(name); \
    if (ret == 0) {                                    \
        map[name[0]] = out;                            \
    }                                                  \

    CALL_ENV_HELPER((std::vector<std::string>{"glxinfo"}))
    CALL_ENV_HELPER((std::vector<std::string>{"uname", "-a"}))
    CALL_ENV_HELPER((std::vector<std::string>{"lspci", "-nn"}))

#undef CALL_ENV_HELPER

    return map;
}

}

int main(int argc, char ** argv) {
    Options opts{};
    try {
        opts.parse_args(argc, argv);
    } catch (OptionException &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    const int total_threads = UTILITY_THREADS + opts.threads;
    const std::map<std::string, std::string> env = getEnvironment();
    auto queue = std::make_shared<testQueue>();
    auto back = std::make_shared<backend::JsonBackend>(opts.resultdir, env);
    auto log = std::make_shared<logQueue>();
    auto ogl = test::piglit::makeOGLInfo(opts.platform);
    auto info = test::SystemInfo(ogl);

    back->initialize(opts);

    std::unique_ptr<unsigned long> count = countTests(opts);
    std::thread logger(simpleLogger, log, std::move(count));

    std::thread workers[total_threads];
    workers[0] = std::thread(readProfiles, queue, opts);

    RunTime time{};
    time.recordStart();
    for (int i = 1; i < total_threads; ++i) {
        workers[i] = std::thread(runTests, queue, back, log, opts, info);
    }
    for (int i = 0; i < total_threads; ++i) {
        workers[i].join();
    }
    time.recordStop();
    log->close();
    logger.join();
    back->finalize(time);

    return 0;
}
